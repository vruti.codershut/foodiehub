<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('orders', function (Blueprint $table) {
          
            $table->string('orderitem_id')->after('ulid');
            $table->string('mobile_number')->after('orderitem_id');
            $table->string('address')->after('mobile_number');
            $table->string('country')->after('address');
            $table->string('state')->after('country');
            $table->string('zipcode')->after('state');
            $table->string('status')->comment('1:order,2:accept,3:ready,4:arriving,5:completed,6:reject')->default('1')->after('zipcode');
            $table->string('otp')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('orderitem_id')->after('ulid');
            $table->dropColumn('mobile_number')->after('orderitem_id');
            $table->dropColumn('address')->after('mobile_number');
            $table->dropColumn('country')->after('address');
            $table->dropColumn('state')->after('country');
            $table->dropColumn('zipcode')->after('state');
            $table->dropColumn('status')->comment('1:order,2:accept,3:ready,4:arriving,5:completed,6:reject')->default('1')->after('zipcode');
            $table->dropColumn('otp')->after('status');
        });
    }
};
