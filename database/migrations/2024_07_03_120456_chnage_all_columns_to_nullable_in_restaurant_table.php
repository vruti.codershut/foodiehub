<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('restaurant', function (Blueprint $table) {
            $table->string('restaurant_name')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('city')->nullable()->change();
            $table->string('state')->nullable()->change();
            $table->string('mobile_number')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('opening_hours')->nullable()->change();
            $table->string('closing_hours')->nullable()->change();
          
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('restaurant', function (Blueprint $table) {
            $table->string('restaurant_name')->change();
            $table->string('address')->change();
            $table->string('city')->change();
            $table->string('state')->change();
            $table->string('mobile_number')->change();
            $table->string('email')->change();
            $table->string('opening_hours')->change();
            $table->string('closing_hours')->change();
            
        });
    }
};
