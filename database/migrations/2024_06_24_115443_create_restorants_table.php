<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('restorants', function (Blueprint $table) {
            $table->id();
            $table->ulid();
            $table->integer('user_id');
            $table->string('restaurant_name');
            $table->string('restaurant_image')->nullable();
            $table->string('type_of_cuisine'); //category_id
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('mobile_number');
            $table->string('email');
            $table->string('website')->nullable();
            $table->string('opening_hours');
            $table->string('closing_hours');
            $table->string('discount')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('restorants');
    }
};
