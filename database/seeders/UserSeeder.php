<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker=Faker::create();

        for ($i = 1; $i <= 1; $i++) {
            $user = new User();
            $user->ulid = Str::ulid();
            $user->name = 'Admin';
            $user->email  = 'admin@gmail.com';
            $user->password = '12345678';
            $user->role = '1';
            $user->approval = '1';
            $user->save();
        }
    }
}
