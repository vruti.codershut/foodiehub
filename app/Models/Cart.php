<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [

       'user_id',
       'restaurant_id',
       'category_id',
       'menu_id',
       'quantity',
      
    ];


    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'categories_id','id');
    }


    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
}
