<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    use HasFactory;

protected $table= "restaurant";

    protected $fillable =
    [
        'ulid',
        'user_id',
        'restaurant_name',
        'restaurant_image',
        'address',
        'city',
        'state',
        'mobile_number',
        'email',
        'website',
        'opening_hours',
        'closing_hours',
        'discount'
    ];

    

    public function restaurantCategory()
    {
        return $this->hasMany(restaurantCategory::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function orders()
    {
        return $this->hasMany(Order::class);
    }


}
