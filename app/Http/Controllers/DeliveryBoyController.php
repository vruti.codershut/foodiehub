<?php

namespace App\Http\Controllers;

use App\Models\DeliveryBoy;
use App\Models\Order;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;
use Illuminate\Validation\Rules;
use Illuminate\Support\Str;

class DeliveryBoyController extends Controller
{
    public function index(Request $request)
    {
        try {
            $selectedColumn = $request->selectedColumn ?? 'id';
            $sortDirection = $request->sortDirection ?? 'ASC';
            $rowPerPage = $request->row_per_page ?? 10;
            $searchData = $request->searchData;

            $query = User::where('role',4);

            if ($searchData) {
                $query->where('name', 'like', "%$searchData%")
                    ->orWhere('email', 'like', "%$searchData%");
            }

            $deliveryBoys = $query->orderBy($selectedColumn, $sortDirection)
                ->paginate($rowPerPage)
                ->withQueryString();

            return Inertia::render('Deliveryboy/Index', [
                'deliveryBoys' => $deliveryBoys,
            ]);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error fetching delivery boys: ' . $e->getMessage()]);
        }
    }

    public function approve(String $ulid)
    {
        try {
            $deliveryBoy = User::where('ulid', $ulid)->firstOrFail();
            $deliveryBoy->approval = 1;
            $deliveryBoy->save();
            return redirect()->redirect()->back()->with(['status' => 'success', 'message' => 'Delivery boy approved successfully']);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error approving delivery boy: ' . $e->getMessage()]);
        }
    }

    public function reject(String $ulid)
    {
        try {
            $deliveryBoy = User::where('ulid', $ulid)->firstOrFail();
            $deliveryBoy->approval = 2;
            $deliveryBoy->save();
            return redirect()->back()->with(['status' => 'success', 'message' => 'Delivery boy rejected successfully']);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error rejecting delivery boy: ' . $e->getMessage()]);
        }
    }

    public function create()
    {
        return Inertia::render('Deliveryboy/Create');
    }

    public function store(Request $request)
    {
      
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:' . User::class,
                'password' => ['required', Rules\Password::defaults()],
            ]);
            try {
            $user = new User();
            $user->ulid = Str::ulid();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->role = 4;
            $user->approval = 0;
            $user->save();

            return redirect()->route('deliveryBoy.show')->with(['status' => 'success', 'message' => 'Delivery boy added successfully']);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error creating delivery boy: ' . $e->getMessage()]);
        }
    }

    public function edit(string $ulid)
    {
        try {
            $deliveryBoy = User::where('ulid', $ulid)->where('role',4)->firstOrFail();
            return Inertia::render('Deliveryboy/Edit', [
                'deliveryBoy' => $deliveryBoy,
            ]);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error fetching delivery boy details: ' . $e->getMessage()]);
        }
    }

    public function update(Request $request,$ulid)
    {
       
            $user = User::where('ulid', $ulid)->where('role',4)->first();
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
                'password' => ['required', Rules\Password::defaults()],
            ]);
            try {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            return redirect()->route('deliveryBoy.show')->with(['status' => 'success', 'message' => 'Delivery boy updated successfully']);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->withInput()->with(['status' => 'error', 'message' => 'Error updating delivery boy: ' . $e->getMessage()]);
        }
    }

    public function delete(string $ulid)
    {
        try {
            User::where('ulid', $ulid)->where('role',4)->delete();
            return redirect()->route('deliveryBoy.show')->with(['status' => 'success', 'message' => 'Delivery boy deleted successfully']);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error deleting delivery boy: ' . $e->getMessage()]);
        }
    }

    public function confirmed(string $id)
    {
        try {
            $order = Order::where('id', $id)->firstOrFail();
            $deliveryBoy = new DeliveryBoy();
            $deliveryBoy->user_id = Auth::user()->id;
            $deliveryBoy->restaurant_id = $order->restaurant_id;
            $deliveryBoy->order_id = $id;
            $deliveryBoy->status = 2;
            $deliveryBoy->code = random_int(1000000, 9999999);
            $deliveryBoy->save();

            $order->inventory_id = 1;
            $order->save();

            return redirect()->route('order.show')->with(['status' => 'success', 'message' => 'Delivery boy order confirmed successfully']);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error confirming delivery boy order: ' . $e->getMessage()]);
        }
    }

    public function confirmOrder(string $id)
    {
        try {
            $deliveryBoy = DeliveryBoy::where('user_id',$id)->firstOrFail();
            $orders = Order::with('user', 'orderItems')->where('id', $deliveryBoy->order_id)->get();

            return Inertia::render('Order/Show', [
                'orders' => $orders,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'warning', 'message' => 'Order not confirmed: ' . $e->getMessage()]);
        }
    }
}
