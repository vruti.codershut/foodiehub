<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\RestaurantCategory;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Support\Facades\Log;

class MenuController extends Controller
{

    public function index(Request $request)
    {

        try {

            $auth = Auth::user()->load('restaurant');
            // Retrieve the restaurant owned by the authenticated user
            // $restaurant = Restaurant::where('user_id', Auth::id())->first();

            // Initialize default values for sorting and pagination
            $selectedColumn = $request->selectedColumn ?? 'id';
            $sortDirection = $request->sortDirection ?? 'ASC';
            $rowPerPage = $request->row_per_page ?? 10;
            $searchData = $request->searchData;

            // Query menus associated with the restaurant, including category and restaurant details
            $query = Menu::with('category', 'restaurant')->where('restaurant_id', $auth->restaurant->id);

            // Apply search filter if searchData is provided
            if ($searchData) {
                $query->where('dish_name', 'like', "%$searchData%");
            }

            // Retrieve menus with sorting and pagination
            $menus = $query->orderBy($selectedColumn, $sortDirection)
                ->paginate($rowPerPage)
                ->withQueryString();

            // Render the Inertia view with menus data
            return Inertia::render('Menu/Index', [
                'menus' => $menus,
            ]);
        } catch (Exception $e) {
           Log::error($e->getMessage());

            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function create()
    {
        try {
            // Retrieve the restaurant owned by the authenticated user
            $auth = Auth::user()->load('restaurant');

            // Retrieve categories associated with the restaurant
            $categoryNames = RestaurantCategory::with('category')
                ->where('restaurant_id', $auth->restaurant->id)
                ->get()
                ->map(function ($restaurantCategory) {
                    return [
                        'id' => $restaurantCategory->category->id,
                        'category_name' => $restaurantCategory->category->category_name
                    ];
                });

            // Render the Inertia view for menu creation with category names and restaurant ID
            return Inertia::render('Menu/Create', [
                'categoryName' => $categoryNames,
                'restaurant_id' => $auth->restaurant->id,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function store(Request $request)
    {

        // Validate request data
        $request->validate([
            'category' => 'required',
            'items.*.name' => 'required',
            'items.*.price'  => 'required',
            'items.*.description' => 'required',
        ]);
        try {
            // Store each item in the menu
            foreach ($request->items as $data) {
                $menu = new Menu();
                $menu->restaurant_id = $request->id;
                $menu->categories_id = $request->category;
                $menu->dish_name = $data['name'];
                $menu->dish_description = $data['description'];
                $menu->price = $data['price'];
                $menu->save();
            }

            // Redirect with success message after storing menus
            return redirect()->route('menu.show')->with(['status' => 'success', 'message' => 'Menu added successfully']);
        } catch (Exception $e) {

            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function edit(String $id)
    {
        try {
            // Find the menu item to edit
            $menu = Menu::find($id);
            if (!$menu) {
                return redirect()->route('menu.show')->with(['status' => 'error', 'message' => 'Menu not found']);
            }
                return Inertia::render('Menu/Edit', [
                    'menu' => $menu,
                ]);
        
            // Render the Inertia view for editing menu item

        } catch (Exception $e) {
            return redirect()->route('menu.show')->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function update(Request $request, $id)
    {

        // Validate request data
        $request->validate([
            'name' => 'required',
            'price'  => 'required',
            'description' => 'required',
        ]);
        try {
            // Find the menu item to update
            $menu = Menu::find($id);
            if (!$menu) {
                return redirect()->route('menu.show')->with(['status' => 'error', 'message' => 'Menu not found']);
            }
            // Update menu item details
            $menu->dish_name = $request->name;
            $menu->dish_description = $request->description;
            $menu->price = $request->price;
            $menu->save();

            return redirect()->route('menu.show')->with(['status' => 'error', 'message' => 'Menu update successfully']);
            // Redirect with success message after updating menu item
        } catch (Exception $e) {
           Log::error($e->getMessage());
            // Output the error message for debugging
            return redirect()->route('menu.show')->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function destroy(string $id)
    {
        try {
            // Delete the menu item
            Menu::find($id)->delete();

            // Redirect with success message after deleting menu item
            return redirect()->route('menu.show')->with(['status' => 'success', 'message' => 'Menu item deleted successfully']);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }
}
