<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Restaurant;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function index()
    {
        try {
            // Logging a test message
            Log::info('This is a test log message');

            // Retrieving authenticated user
            $auth = Auth::user();

            // Counting different types of users and orders
            $data = [
                'customer' => User::where('role', 3)->count(),
                'restaurant' => User::where('role', 2)->count(),
                'deliveryboy' => User::where('role', 4)->count(),
                'rejectorder' => Order::where('status', 6)->count(),
                'totalorder' => Order::count(),
                'completedorder' => Order::where('status', 5)->count(),
            ];

            // Displaying dashboard based on user role
            if ($auth->role == 4) {
                return Inertia::render('DeliveryboyDashboard', [
                    'data' => $data,
                ]);
            } elseif ($auth->role == 3) {
                $restaurants = Restaurant::all();
                return Inertia::render('CustomerDashboard', [
                    'restaurants' => $restaurants,
                ]);
            } elseif ($auth->role == 2) {
                return Inertia::render('RestaurantDashboard', [
                    'data' => $data,
                ]);
            } else {
                // Assuming admin role (role == 1)
                return Inertia::render('AdminDashboard', [
                    'data' => $data,
                ]);
            }
        } catch (Exception $e) {
           
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }
}
