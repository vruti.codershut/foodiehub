<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
use Inertia\Response;

class NewPasswordController extends Controller
{
    /**
     * Display the password reset view.
     */
    public function create(Request $request): Response
    {
        return Inertia::render('Auth/ResetPassword', [
            'email' => $request->email,
            'token' => $request->route('token'),
        ]);
    }

    /**
     * Handle an incoming new password request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'token' => 'required',
            'email' => 'required|string|lowercase|email|max:255',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);
    
        // Retrieve the user by email
        $user = User::where('email',$request->email)->first();
    
        // Check if the user exists
        if (!$user) {
            return redirect()->route('password.reset', ['token' => $request->token])->with(['status' => 'warning','message' =>'Email not found']);
        }
        
    
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => bcrypt($password),
                    'remember_token' => Str::random(60),
                ])->save();
    
                event(new PasswordReset($user));
                
            }
        );
    
        // Check the status of the password reset attempt
        if ($status == Password::PASSWORD_RESET) {
            return redirect()->route('login', ['token' => $request->token])->with(['status' => 'success','message' =>'Password reset successfully']);
        }
    
        throw ValidationException::withMessages([
            'email' => [trans($status)],
        ]);
    }
}
