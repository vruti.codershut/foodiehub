<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Inertia\Inertia;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        try {
            $selectedColumn = $request->selectedColumn ?? 'id';
            $sortDirection = $request->sortDirection ?? 'ASC';
            $rowPerPage = $request->row_per_page ?? 10;
            $searchData = $request->searchData;

            $query = Category::query();
            if ($searchData) {
                $query->where('category_name', 'like', "%$searchData%");
            }

            $categories = $query->orderBy($selectedColumn, $sortDirection)
                ->paginate($rowPerPage)
                ->withQueryString();

            return Inertia::render('Category/Index', [
                'categories' => $categories,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return back()->with(['status' => 'error', 'message' => 'Error fetching categories: ' . $e->getMessage()]);
        }
    }

    public function create()
    {
        return Inertia::render('Category/Create');
    }

    public function store(Request $request)
    {

        $request->validate([
            'category_name' => 'required|string|max:255',
            'category_desciption' => 'required|string|max:255',
        ]);
        try {
            $category = new Category();
            $category->ulid = Str::ulid();
            $category->category_name = $request->category_name;
            $category->category_desciption = $request->category_desciption;
            $category->save();

            return redirect()->route('category.show')->with(['status' => 'success', 'message' => 'Category added successfully']);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error creating category: ' . $e->getMessage()]);
        }
    }

    public function edit(string $ulid)
    {
        try {
            $category = Category::where('ulid', $ulid)->firstOrFail();
            return Inertia::render('Category/Edit', [
                'category' => $category,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return  redirect()->back()->with(['status' => 'error', 'message' => 'Error editing category: ' . $e->getMessage()]);
        }
    }

    public function update(Request $request, $ulid)
    {

        $request->validate([
            'category_name' => 'required|string|max:255',
            'category_desciption' => 'required|string|max:255',
        ]);
        try {
            $Category = Category::where('ulid', $ulid)->firstOrFail();
            $Category->category_name = $request->category_name;
            $Category->category_desciption = $request->category_desciption;
            $Category->save();

            return redirect()->route('category.show')->with(['status' => 'success', 'message' => 'Category updated successfully']);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error updating category: ' . $e->getMessage()]);
        }
    }

    public function delete(string $ulid)
    {
        try {
            Category::where('ulid', $ulid)->delete();
            return redirect()->route('category.show')->with(['status' => 'success', 'message' => 'Category deleted successfully']);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error deleting category: ' . $e->getMessage()]);
        }
    }
}
