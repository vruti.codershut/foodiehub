<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Restaurant;
use App\Models\RestaurantCategory;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;
use Inertia\Inertia;

class RestaurantController extends Controller
{

    /**
     * Display a listing of the restaurants.
     */
    public function index(Request $request)
    {
        try {
            $restaurants = Restaurant::with('user', 'RestaurantCategory', 'RestaurantCategory.category');

            // Filter by search data if provided
            if ($request->searchData) {
                $restaurants->orWhereHas('user', function ($q) use ($request) {
                    $q->where('name', 'like', "%$request->searchData%")
                      ->orWhere('email', 'like', "%$request->searchData%");
                });
            }

            // Handle sorting based on selected column
            $selectedColumn = $request->selectedColumn ?? 'id';
            $sortDirection = $request->sortDirection ?? 'ASC';
            $rowPerPage = $request->row_per_page ?? 10;

            if (in_array($selectedColumn, ['name', 'email'])) {
                $restaurants->join('users', 'restaurants.user_id', '=', 'users.id')
                      ->orderBy('users.' . str_replace('user_', '', $selectedColumn), $sortDirection);
            } else {
                $restaurants->orderBy($selectedColumn, $sortDirection);
            }

            // Paginate the results
            $restaurants = $restaurants->paginate($rowPerPage)->withQueryString();

            // Render Inertia view with restaurants data
            return Inertia::render('Restaurant/Index', [
                'restaurants' => $restaurants,
            ]);
        } catch (Exception $e) {
            // Log the exception and handle error gracefully
            Log::error('Error fetching restaurant list: ' . $e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Approve a restaurant by setting approval status to 1.
     */
    public function approve(String $ulid)
    {
        try {
            $user = User::where('ulid', $ulid)->firstOrFail();
            $user->approval = 1;
            $user->save();

            return redirect()->back()->with(['status' => 'success', 'message' => 'Restaurant approved successfully']);
        } catch (Exception $e) {
            // Log the exception and handle error gracefully
            Log::error('Error approving restaurant: ' . $e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Reject a restaurant by setting approval status to 2.
     */
    public function reject(String $ulid)
    {
        try {
            $user = User::where('ulid', $ulid)->firstOrFail();
            $user->approval = 2;
            $user->save();

            return redirect()->back()->with(['status' => 'success', 'message' => 'Restaurant rejected successfully']);
        } catch (Exception $e) {
            // Log the exception and handle error gracefully
            Log::error('Error rejecting restaurant: ' . $e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' =>$e->getMessage()]);
        }
    }

    /**
     * Show the form for creating a new restaurant.
     */
    public function create()
    {
        try {
            // Fetch all categories for restaurant creation form
            $categories = Category::select('id', 'category_name')->get();

            // Render Inertia view with category data
            return Inertia::render('Restaurant/Create', [
                'categories' => $categories,
            ]);
        } catch (Exception $e) {
            // Log the exception and handle error gracefully
            Log::error('Error fetching categories for restaurant creation: ' . $e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Store a newly created restaurant in storage.
     */
    public function store(Request $request)
    {
       
            // Validate incoming request data
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|lowercase|email|max:255|unique:' . User::class,
                'password' => ['required', Rules\Password::defaults()],
                'restaurant_name' => 'required|string|max:255',
                'restaurant_image' => 'nullable|image|max:1024',
                'address' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'state' => 'required|string|max:255',
                'mobileNumber' => 'required|string|max:15',
                'restaurantEmail' => 'required|string|email|max:255',
                'website' => 'nullable|string|url|max:255',
                'openingHours' => 'required|date_format:H:i',
                'closingHours' => 'required|date_format:H:i',
                'categories' => 'required|array',
            ]);
            try {
            // Create new user
            $user = new User();
            $user->ulid = Str::ulid();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->role = 2;
            $user->approval = 0;
            $user->save();

            // Create new restaurant
            $restaurant = new Restaurant();
            $restaurant->ulid = Str::ulid();
            $restaurant->user_id = $user->id;
            $restaurant->restaurant_name = $request->restaurant_name;
            $restaurant->address = $request->address;
            $restaurant->city = $request->city;
            $restaurant->state = $request->state;
            $restaurant->mobile_number = $request->mobileNumber;
            $restaurant->email = $request->restaurantEmail;
            $restaurant->website = $request->website;
            $restaurant->opening_hours = $request->openingHours;
            $restaurant->closing_hours = $request->closingHours;

            // Store restaurant image if provided
            if ($request->hasFile('restaurant_image')) {
                $imageName = uniqid() . '.' . $request->restaurant_image->extension();
                $request->restaurant_image->storeAs('public/restaurant', $imageName);
                $restaurant->restaurant_image = $imageName;
            }

            $restaurant->save();

            // Attach categories to the restaurant
            foreach ($request->categories as $categoryId) {
                $restaurantCategory = new RestaurantCategory();
                $restaurantCategory->restaurant_id = $restaurant->id;
                $restaurantCategory->category_id = $categoryId;
                $restaurantCategory->save();
            }

            return redirect()->route('restaurant.show')->with(['status' => 'success', 'message' => 'Restaurant added successfully']);
        } catch (Exception $e) {
            // Log the exception and handle error gracefully
            Log::error('Error adding restaurant: ' . $e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified restaurant.
     */
    public function edit(string $ulid)
    {
        try {
            // Fetch restaurant details and associated categories for editing
            $restaurant = Restaurant::with('user', 'RestaurantCategory')->where('ulid', $ulid)->firstOrFail();
            $selectedCategories = $restaurant->RestaurantCategory->pluck('category_id');

            // Fetch all categories for the edit form
            $categories = Category::select('id', 'category_name')->get();

            // Render Inertia view with restaurant and category data
            return Inertia::render('Restaurant/Edit', [
                'restaurant' => $restaurant,
                'categories' => $categories,
                'selectedCategories' => $selectedCategories,
            ]);
        } catch (Exception $e) {
            // Log the exception and handle error gracefully
            Log::error('Error fetching restaurant for editing: ' . $e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified restaurant in storage.
     */
    public function update(Request $request,$ulid)
    {
      
            // Validate incoming request data
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|email|unique:users,email,' . $request->id,
                'password' => ['nullable', Rules\Password::defaults()],
                'restaurant_name' => 'required|string|max:255',
                'restaurant_image' => 'nullable|image|max:1024',
                'address' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'state' => 'required|string|max:255',
                'mobileNumber' => 'required|string|max:15',
                'restaurantEmail' => 'required|string|email|max:255',
                'website' => 'nullable|string|url|max:255',
                'openingHours' => 'required|date_format:H:i',
                'closingHours' => 'required|date_format:H:i',
                'categories' => 'required|array',
            ]);
            try {
            // Find and update user details
            $user = User::where('ulid', $ulid)->firstOrFail();
            $user->name = $request->name;
            $user->email = $request->email;
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            $user->save();

            // Find and update restaurant details
            $restaurant = Restaurant::where('user_id', $user->id)->firstOrFail();
            $restaurant->restaurant_name = $request->restaurant_name;
            $restaurant->address = $request->address;
            $restaurant->city = $request->city;
            $restaurant->state = $request->state;
            $restaurant->mobile_number = $request->mobileNumber;
            $restaurant->email = $request->restaurantEmail;
            $restaurant->website = $request->website;
            $restaurant->opening_hours = $request->openingHours;
            $restaurant->closing_hours = $request->closingHours;

            // Store updated restaurant image if provided
            if ($request->hasFile('restaurant_image')) {
                $imageName = uniqid() . '.' . $request->restaurant_image->extension();
                $request->restaurant_image->storeAs('public/restaurant', $imageName);
                $restaurant->restaurant_image = $imageName;
            }

            $restaurant->save();

            // Update restaurant categories
            RestaurantCategory::where('restaurant_id', $restaurant->id)->delete();
            foreach ($request->categories as $categoryId) {
                $restaurantCategory = new RestaurantCategory();
                $restaurantCategory->restaurant_id = $restaurant->id;
                $restaurantCategory->category_id = $categoryId;
                $restaurantCategory->save();
            }

            return redirect()->route('restaurant.show')->with(['status' => 'success', 'message' => 'Restaurant updated successfully']);
        } catch (Exception $e) {
            // Log the exception and handle error gracefully
            Log::error('Error updating restaurant: ' . $e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified restaurant from storage.
     */
    public function delete(string $ulid)
    {
        try {
            // Find and delete the restaurant and associated user
            $user = User::where('ulid', $ulid)->firstOrFail();
            $restaurant = Restaurant::where('user_id', $user->id)->firstOrFail();

            // Delete restaurant categories first
            RestaurantCategory::where('restaurant_id', $restaurant->id)->delete();

            // Delete restaurant and user
            $restaurant->delete();
            $user->delete();

            return redirect()->route('restaurant.show')->with(['status' => 'success', 'message' => 'Restaurant deleted successfully']);
        } catch (Exception $e) {
            // Log the exception and handle error gracefully
            Log::error('Error deleting restaurant: ' . $e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }
}
