<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CartController extends Controller
{
    public function index()
    {
        try {
            $user_id = Auth::id();
            $carts = Cart::with('menu')->where('user_id', $user_id)->get();

            return response()->json([
                'status' => true,
                'message' => 'Cart Available',
                'carts' => $carts,
            ], 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $cart = new Cart();
            $cart->user_id = $request->user_id;
            $cart->restaurant_id = $request->restaurant_id;
            $cart->category_id = $request->category_id;
            $cart->menu_id = $request->menu_id;
            $cart->quantity = 1;
            $cart->save();

            return redirect()->back()->with(['status' => 'success', 'message' => 'Added successfully']);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Failed to add: ' . $e->getMessage()]);
        }
    }

    public function update(Request $request)
    {
        $userId = Auth::id();

        try {
            // Find the specific cart item by ID and user
            $cartItem = Cart::where('id', $request->itemId)->where('user_id', $userId)->firstOrFail();

            // Update the cart item attributes based on the request data
            $cartItem->update([
                "quantity" => $request->quantity
            ]);

            // Return the updated cart item
            return response()->json([
                'status' => true,
                'message' => 'Cart item updated successfully',
                'data' => $cartItem,
            ], 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }
    public function delete(string $id)
    {

        try {
            Cart::find($id)->delete();
            return response([
                'status' => 'true',
                'message' => 'cart deletd sucessfully',
            ], 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response([
                'status' => 'false',
                'message' => 'cart Not Add Successfully',
            ], 500);
        }
    }
}
