<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;
use Illuminate\Validation\Rules;
use Illuminate\Support\Str;
use Exception;
use Illuminate\Support\Facades\Log;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        try {
            $selectedColumn = $request->selectedColumn ?? 'id';
            $sortDirection = $request->sortDirection ?? 'ASC';
            $rowPerPage = $request->row_per_page ?? 10;
            $searchData = $request->searchData;

            $query = User::where('role',3);

            if ($searchData) {
                $query->where('name', 'like', "%$searchData%")
                    ->orWhere('email', 'like', "%$searchData%");
            }

            $customers = $query->orderBy($selectedColumn, $sortDirection)
                ->paginate($rowPerPage)
                ->withQueryString();

            return Inertia::render('Customer/Index', [
                'customers' => $customers,
            ]);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return  redirect()->back()->with(['status' => 'error', 'message' => 'Error fetching customers: ' . $e->getMessage()]);
        }
    }

    public function create()
    {
        return Inertia::render('Customer/Create');
    }

    public function store(Request $request)
    {
       
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email',
                'password' => ['required', Rules\Password::defaults()],
            ]);
            try {
            $user = new User();
            $user->ulid = Str::ulid();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->role = 3;
            $user->approval = 1;
            $user->save();

            return redirect()->route('customer.show')->with(['status' => 'success', 'message' => 'Customer added successfully']);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error creating customer: ' . $e->getMessage()]);
        }
    }

    public function edit(string $ulid)
    {
        try {
            $customer = User::where('ulid',$ulid)->where('role',3)->firstOrFail();
            return Inertia::render('Customer/Edit', [
                'customer' => $customer,
            ]);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error fetching customer details: ' . $e->getMessage()]);
        }
    }

    public function update(Request $request,$ulid)
    {
      
            $user = User::where('ulid',$ulid)->where('role',3)->first();
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
                'password' => ['required', Rules\Password::defaults()],
            ]);
            try {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            return redirect()->route('customer.show')->with(['status' => 'success', 'message' => 'Customer updated successfully']);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error updating customer: ' . $e->getMessage()]);
        }
    }

    public function delete(string $ulid)
    {
        try {
            User::where('ulid',$ulid)->where('role',3)->delete();
            return redirect()->route('customer.show')->with(['status' => 'success', 'message' => 'Customer deleted successfully']);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error deleting customer: ' . $e->getMessage()]);
        }
    }

    public function menulist(string $id)
    {
        try {
            $menus = Menu::with('category')->where('restaurant_id', $id)->get()->groupBy('categories_id')->map(function ($menuGroup) {
                return $menuGroup->map(function ($menu) {
                    return [
                        'restaurant_id' => $menu->restaurant_id,
                        'id' => $menu->id,
                        'dish_name' => $menu->dish_name,
                        'price' => $menu->price,
                        'category' => [
                            'id' => $menu->category->id,
                            'category_name' => $menu->category->category_name,
                        ],
                    ];
                });
            });

            return Inertia::render('Customer/Menulist', [
                'menus' => $menus,
            ]);
        } catch (Exception $e) {
           Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error fetching menu list: ' . $e->getMessage()]);
        }
    }
}
