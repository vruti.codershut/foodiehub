<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Menu;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Restaurant;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Illuminate\Support\Str;
use Exception;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        try {
            // Initialize variables for search, sorting, pagination
            $searchName = $request->searchName;
            $searchDate = $request->searchDate;
            $selectedColumn = $request->selectedColumn ?? 'id';
            $sortDirection = $request->sortDirection ?? 'ASC';
            $rowPerPage = $request->row_per_page ?? 10;

            // Start querying orders with user and orderItems relationships
            $orders = Order::with('user','orderItems');
            $auth = Auth::user();

            // Apply filters based on user role and search criteria
            if ($searchName) {
                $orders = $orders->whereHas('user', function ($query) use ($searchName) {
                    $query->where('name', 'like', '%' . $searchName . '%');
                });
            }
            if ($searchDate) {
                $orders = $orders->where('order_date', 'like', $searchDate);
            }
            if ($auth->role == 4) {
                $orders = $orders->where('inventory_id', '0');
            }
            if ($auth->role == 3) {
                $orders = $orders->where('user_id', $auth->id);
            }
            if ($auth->role == 2) {
                $restaurant = Restaurant::where('user_id',$auth->id)->first();
                $orders = $orders->where('restaurant_id', $restaurant->id);
            }

            // Finalize the query with sorting and pagination
            $orders = $orders->orderBy($selectedColumn, $sortDirection)
                ->paginate($rowPerPage)
                ->withQueryString();

            // Render Inertia view with orders data
            return Inertia::render('Order/Index', [
                'orders' => $orders,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => 'Error fetching orders']);
        }
    }

    public function create()
    {
        try {
            $auth=Auth::id();
            // Fetch user and carts for order creation
            $user = User::where('id',$auth)->first();
            $carts = Cart::where('user_id',$auth)->first();

            // Render Inertia view for order checkout with user and cart data
            return Inertia::render('Order/Checkout', [
                'user' => $user,
                'carts' => $carts,
            ]);
        } catch (Exception $e) {
          Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function store(Request $request)
    {
        try {
            // Retrieve carts and calculate total price
            $carts = Cart::where('user_id',$request->user_id)->get();
            $totalprice = 0;
            $orderItems = [];
            $date = Carbon::now();

            // Create a new order instance
            $order = new Order();
            $order->ulid = Str::ulid();
            $order->restaurant_id = $request->restaurant_id;
            $order->user_id = $request->user_id;
            $order->mobile_number = $request->phoneNumber;
            $order->address = $request->address;
            $order->country = $request->country;
            $order->state = $request->state;
            $order->zipcode = $request->zipCode;
            $order->status = 1;
            $order->otp = random_int(100000, 999999);
            $order->inventory_id = 0;
            $order->order_date = $date;
            $order->total_amount = 0;
            $order->payment_method = 1;
            $order->save();

            // Create order items and calculate total price
            foreach ($carts as $cart) {
                $menu = Menu::find($cart->menu_id);
                $orderItem = new OrderItem();
                $orderItem->order_id = $order->id;
                $orderItem->user_id = $cart->user_id;
                $orderItem->restaurant_id = $cart->restaurant_id;
                $orderItem->category_id = $cart->category_id;
                $orderItem->menu_id = $cart->menu_id;
                $orderItem->quantity = $cart->quantity;
                $orderItem->save();
                $totalprice += $menu->price * $cart->quantity;
                $orderItems[] = $orderItem;
            }

            // Update total amount for the order and delete carts
            $order = Order::where('id', $order->id)->first();
            $order->total_amount = $totalprice;
            $order->save();

            // Delete carts after order creation
            Cart::where('user_id', $request->user_id)->delete();

            // Redirect with success message after storing order
            return redirect()->route('order.show')->with(['status' => 'success', 'message' => 'Order added successfully']);
        } catch (Exception $e) {
          Log::error($e->getMessage());
           
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function delete(string $ulid)
    {
        try {
            // Delete order and its associated order items
            $order = Order::where('ulid', $ulid)->first();
            $order->orderItems()->delete();
            $order->delete();
        } catch (Exception $e) {
          Log::error($e->getMessage());
           
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function view(string $ulid)
    {
        try {
            // Fetch order details with associated order items and user
            $order = Order::with(['orderItems.menu' => function ($query) {
                $query->select('id', 'dish_name', 'price');
            }, 'user' => function ($query) {
                $query->select('id', 'name', 'email');
            }])
                ->where('ulid', $ulid)
                ->first();

            // Prepare data for Inertia view
            if ($order) {
                $orderData = [
                    'ulid' => $order->ulid,
                    'userName' => $order->user->name,
                    'userEmail' => $order->user->email,
                    'address' => $order->address,
                    'mobileNumber' => $order->mobile_number,
                    'country' => $order->country,
                    'state' => $order->state,
                    'status' => $order->status,
                    'otp' => $order->otp,
                    'totalAmount' => $order->total_amount,
                    'items' => $order->orderItems->map(function ($orderItem) {
                        return [
                            'dishName' => $orderItem->menu->dish_name,
                            'price' => $orderItem->menu->price,
                            'quantity' => $orderItem->quantity,
                        ];
                    }),
                ];
            } else {
                $orderData = [];
            }

            // Render Inertia view with order data
            return Inertia::render('Order/View', [
                'orders' => $orderData,
            ]);
        } catch (Exception $e) {
          Log::error($e->getMessage());
           
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function accept(string $ulid)
    {
        try {
            // Update order status to next stage (1 -> 2, 2 -> 3, etc.)
            $order = Order::where('ulid', $ulid)->first();
            if ($order->status == 1) {
                $order->status = 2;
            } else if ($order->status == 2) {
                $order->status = 3;
            } else if ($order->status == 3) {
                $order->status = 4;
            } else if ($order->status == 4) {
                $order->status = 5;
            }
            $order->save();
        } catch (Exception $e) {
          Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function reject(string $ulid)
    {
        try {
            // Update order status to rejected (status 6)
            $order = Order::where('ulid', $ulid)->first();
            $order->status = 6;
            $order->save();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }
}
