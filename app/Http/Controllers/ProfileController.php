<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\User;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\RestaurantCategory;
use Exception;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the user's profile.
     */
    public function edit()
    {
        try {
            $auth = Auth::user();

            // Check if user role is not a restaurant owner (role 2)
            if ($auth->role != 2) {
                return Inertia::render('Profile/Edit', [
                    'user' => $auth,
                    'restaurant' => [],
                    'selectedCategories' => [],
                    'categories' => [],
                ]);
            }

            // Fetch restaurant details for restaurant owners (role 2)
            $restaurant = Restaurant::where('user_id', $auth->id)
                ->with('RestaurantCategory.category')
                ->first();

            // Redirect back if restaurant data is not found
            if (!$restaurant) {
                return redirect()->back();
            }

            // Prepare selected category IDs and all categories
            $selectedCategories = $restaurant->RestaurantCategory->pluck('category.id');
            $categories = Category::select('id', 'category_name')->get();

            // Render Inertia view with user, restaurant, and category data
            return Inertia::render('Profile/Edit', [
                'user' => $auth,
                'restaurant' => $restaurant,
                'selectedCategories' => $selectedCategories,
                'categories' => $categories,
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage());
           
            return redirect()->back()->with(['status' => 'error', 'message' =>$e->getMessage()]);
        }
    }

    /**
     * Update the user's profile information.
     */
    public function update(Request $request)
    {
       
            $user = User::where('id', Auth::user()->id)->first();

            // Validate based on the update key (1, 2, 3)
            if ($request->key == 1) {
                $request->validate([
                    'name' => 'required|string|max:255',
                    'email' => 'required|email|unique:users,email,' . $user->id,
                ]);
            } elseif ($request->key == 2) {
                $request->validate([
                    'currentpassword' => ['required', 'nullable'],
                    'password' => ['required', 'confirmed', 'nullable'],
                    'password_confirmation' => 'required',
                ]);
            } else {
                $request->validate([
                    'restaurant_name' => 'required|string|max:255',
                    'restaurant_image' => 'nullable|image|max:1024',
                    'address' => 'required|string|max:255',
                    'city' => 'required|string|max:255',
                    'state' => 'required|string|max:255',
                    'mobileNumber' => 'required|string|max:15',
                    'restaurantEmail' => 'required|string|email|max:255',
                    'website' => 'nullable|string|url|max:255',
                    'openingHours' => 'required|date_format:H:i',
                    'closingHours' => 'required|date_format:H:i',
                    'categories' => 'required|array',
                ]);
            }
            try {
            // Update user's general information
            $user->name = $request->name ?? Auth::user()->name;
            $user->email = $request->email ?? Auth::user()->email;
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            $user->save();

            // Update restaurant information
            $restaurant = Restaurant::where('user_id', $user->id)->first();
            $restaurant->restaurant_name = $request->restaurant_name;
            $restaurant->address = $request->address;
            $restaurant->city = $request->city;
            $restaurant->state = $request->state;
            $restaurant->mobile_number = $request->mobileNumber;
            $restaurant->email = $request->restaurantEmail;
            $restaurant->website = $request->website;
            $restaurant->opening_hours = $request->openingHours;
            $restaurant->closing_hours = $request->closingHours;
            $restaurant->save();

            // Update restaurant categories
            RestaurantCategory::where('restaurant_id', $restaurant->id)->delete();
            foreach ($request->categories as $categoryId) {
                $restaurantCategory = new RestaurantCategory();
                $restaurantCategory->restaurant_id = $restaurant->id;
                $restaurantCategory->category_id = $categoryId;
                $restaurantCategory->save();
            }

            // Redirect with success message after profile update
            return redirect()->route('dashboard')->with(['status' => 'success', 'message' => 'User profile updated successfully']);
        } catch (Exception $e) {
            Log::error($e->getMessage());
           
            return redirect()->back()->with(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }
}
