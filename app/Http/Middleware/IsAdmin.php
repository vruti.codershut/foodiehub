<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
       
        if (Auth::check())
         {
            if(Auth::user()->role == 4)
            {
                return redirect()->route('DeliveryboyDashboard');
            }
           
            if(Auth::user()->role == 3)
            {
                return redirect()->route('CustomerDashboard');
            }
            if(Auth::user()->role == 2)
            {
                return redirect()->route('RestaurantDashboard');
            }
            else
            {
                return redirect()->route('AdminDashboard');
             }
       }
        return redirect()->route('login');

        return $next($request);
    }
  
}
