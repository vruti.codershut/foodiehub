<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class RoleMiddleware
{
   
     /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */

     public function handle(Request $request, Closure $next, $role = null)
     {
      
         if (Auth::check() && Auth::user()->role != $role) {
           
             if ($request->is('menu*')) {
                 return redirect()->back();
             }
         }
  
         return $next($request);
     }
}
