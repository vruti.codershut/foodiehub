
import { router } from "@inertiajs/vue3";
import Swal from "sweetalert2";


export function useGeneral() {

 /**
    * Generates a complete asset path by combining the base asset path and the provided relative path.
    * @param {string} path - The relative path of the asset.
    * @returns {string} - A complete asset path.
    */
 const asset = (path) => {
   const base_path = window._asset || '';
   return base_path + path;
}

const deleteFunction=(route)=>
{
   Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
  }).then((result) => {
      if (result.isConfirmed) {
          router.get(route);
      }
  });
}

const approvalFunction=(route)=>
   { 
      Swal.fire({
         title: "Are you sure?",
         text: "You want to approve",
         icon: "warning",
         showCancelButton: true,
         confirmButtonColor: "#3085d6",
         cancelButtonColor: "#d33",
         confirmButtonText: "Yes, approve it!",
     }).then((result) => {
         if (result.isConfirmed) {
             router.get(route);
         }
     });
   }
   const rejectFunction=(route)=>
      {
         Swal.fire({
            title: "Are you sure?",
            text: "You want to reject",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, reject it!",
        }).then((result) => {
            if (result.isConfirmed) {
                router.get(route);
            }
        });
      }

return {
   asset,
   deleteFunction,
   approvalFunction,
   rejectFunction,

}

}