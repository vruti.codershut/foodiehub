<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\RestaurantController;
use App\Http\Controllers\DeliveryBoyController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;



Route::middleware('IsAdmin')->get('/', function () {
    return redirect()->route('login');
});

Route::get('/', function () {
    return redirect()->route('dashboard');
});

Route::middleware('auth')->controller(HomeController::class)->prefix('dashboard')->group(function () {
    Route::get('/', 'index')->name('dashboard');
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::post('/profile-update', [ProfileController::class, 'update'])->name('profile.update');
});

Route::middleware('auth')->controller(CustomerController::class)->prefix('customers')->group(function () {
    Route::get('/', 'index')->name('customer.show');
    Route::get('/create', 'create')->name('customer.create');
    Route::post('/store', 'store')->name('customer.store');
    Route::get('/edit/{ulid}', 'edit')->name('customer.edit');
    Route::post('/update/{ulid}', 'update')->name('customer.update');
    Route::get('/delete/{ulid}', 'delete')->name('customer.delete');
    Route::get('/approve/{ulid}', 'approve')->name('customer.approve');
    Route::get('/reject/{ulid}', 'reject')->name('customer.reject');
    Route::get('/menu-list/{id}', 'menulist')->name('customer.menu');
});

Route::middleware('auth')->controller(CartController::class)->prefix('/cart')->group(function () {

    Route::get('/', 'index')->name('api.cart.list');
    Route::post('/add_to_cart', 'store')->name('cart.store');
    Route::post('/update', 'update')->name('api.cart.update');
    Route::get('/delete/{id}', 'delete')->name('api.cart.delete');
});

Route::middleware('auth')->controller(OrderController::class)->prefix('order')->group(function () {
    Route::get('/', 'index')->name('order.show');
    Route::get('/create', 'create')->name('order.create');
    Route::post('/store', 'store')->name('order.store');
    Route::get('/delete/{ulid}', 'delete')->name('order.delete');
    Route::get('/accept/{ulid}', 'accept')->name('order.accept');
    Route::get('/view/{ulid}', 'view')->name('order.view');
    Route::get('/reject/{ulid}', 'reject')->name('order.reject');
});

Route::middleware('auth')->controller(RestaurantController::class)->prefix('restaurants')->group(function () {
    Route::get('/', 'index')->name('restaurant.show');
    Route::get('/create', 'create')->name('restaurant.create');
    Route::post('/store', 'store')->name('restaurant.store');
    Route::get('/edit/{ulid}', 'edit')->name('restaurant.edit');
    Route::post('/update/{ulid}', 'update')->name('restaurant.update');
    Route::get('/delete/{ulid}', 'delete')->name('restaurant.delete');
    Route::get('/approve/{ulid}', 'approve')->name('restaurant.approve');
    Route::get('/reject/{ulid}', 'reject')->name('restaurant.reject');
});

Route::middleware('auth')->controller(DeliveryBoyController::class)->prefix('delivery-boys')->group(function () {
    Route::get('/', 'index')->name('deliveryBoy.show');
    Route::get('/create', 'create')->name('deliveryBoy.create');
    Route::post('/store', 'store')->name('deliveryBoy.store');
    Route::get('/edit/{ulid}', 'edit')->name('deliveryBoy.edit');
    Route::post('/update/{ulid}', 'update')->name('deliveryBoy.update');
    Route::get('/delete/{ulid}', 'delete')->name('deliveryBoy.delete');
    Route::get('/approve/{ulid}', 'approve')->name('deliveryBoy.approve');
    Route::get('/reject/{ulid}', 'reject')->name('deliveryBoy.reject');
    Route::get('/confirmed/{id}', 'confirmed')->name('deliveryBoy.confirmed');
    Route::get('/confirm-order/{id}', 'confirmOrder')->name('deliveryBoy.order');
});


Route::middleware(['auth', 'role:2'])->controller(MenuController::class)->prefix('menu')->group(function () {
    Route::get('/', 'index')->name('menu.show');
    Route::get('/create', 'create')->name('menu.create');
    Route::post('/store', 'store')->name('menu.store');
    Route::get('/edit/{id}', 'edit')->name('menu.edit');
    Route::post('/update/{id}', 'update')->name('menu.update');
    Route::get('/delete/{id}', 'destroy')->name('menu.delete');
});



Route::middleware('auth')->controller(CategoryController::class)->prefix('category')->group(function () {
    Route::get('/','index')->name('category.show');
    Route::get('/create', 'create')->name('category.create');
    Route::post('/store', 'store')->name('category.store');
    Route::get('/edit/{ulid}', 'edit')->name('category.edit');
    Route::post('/update/{ulid}', 'update')->name('category.update');
    Route::get('/delete/{ulid}', 'delete')->name('category.delete');
});

require __DIR__ . '/auth.php';
